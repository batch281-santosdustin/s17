/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function printDetails() {
	let firstName = prompt("Enter your First Name: ");
	let lastName = prompt("Enter your Last Name: ");
	let getAge = prompt("Enter your Age: ");
	let getLocation = prompt("Enter your location: ");

	alert("Thank you");

	console.log("Hello, " + firstName + " " + lastName);
	console.log("You are " + getAge + " years old.");
	console.log("You live in " + getLocation);
}

printDetails();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function printFavoriteBands() {
	let firstArtist = "P!nk";
	let secondArtist = "Panic! at the Disco";
	let thirdArtist = "Tom Grennan";
	let fourthArtist = "Calum Scott";
	let fifthArtist = "Paramore";

	console.log("1. " + firstArtist);
	console.log("2. " + secondArtist);
	console.log("3. " + thirdArtist);
	console.log("4. " + fourthArtist);
	console.log("5. " + fifthArtist);
}

printFavoriteBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function printFavoriteMovie() {
	let firstMovie = "Harry Potter and the Deathly Hallows Part 2";
	let secondMovie = "Avengers Infinity War";
	let thirdMovie = "Everything, Everywhere, All at Once";
	let fourthMovie = "Parasite";
	let fifthMovie = "Split";

	let ratingOne = 96;
	let ratingTwo = 85;
	let ratingThree = 94;
	let ratingFour = 99;
	let ratingFive = 78;

	console.log("1. " + firstMovie);
	console.log("Rotten Tomatoes Rating: " + ratingOne + "%");
	console.log("2. " + secondMovie);
	console.log("Rotten Tomatoes Rating: " + ratingTwo + "%");
	console.log("3. " + thirdMovie);
	console.log("Rotten Tomatoes Rating: " + ratingThree + "%");
	console.log("4. " + fourthMovie);
	console.log("Rotten Tomatoes Rating: " + ratingFour + "%");
	console.log("5. " + fifthMovie);
	console.log("Rotten Tomatoes Rating: " + ratingFive + "%");
}

printFavoriteMovie();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name: "); 
	let friend2 = prompt("Enter your second friend's name: "); 
	let friend3 = prompt("Enter your third friend's name: ");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

