// Function
/*
	Syntax:
		function functionName() {
			code block (statement)
		}
*/

function printName() {
	console.log("My name is Dustin");
};

printName();

// Function Declaration vs expressions

// Function declaration
function declaredFunction() {
	console.log("Hello, world from declaredFunction()");
};

declaredFunction();

// Function expression
let variableFunction = function() {
	console.log("Hello again!");
};

variableFunction();

declaredFunction = function() {
	console.log("Updated declaredFunction");
};

declaredFunction();

const	constantFunc = function() {
	console.log("Initialized with const!");
};

constantFunc();

/*constantFunc = function () {
	console.log("Reassigned");
};

constantFunc();*/

// Function Scoping

/*
	Scope is the accessibility (visibility) of variables

	JS Variables has 3 types of scope:
		1. Local/ Block Scope
		2. Global Scope
		3. Function Scope
*/

{
	let localVar = "Alonzo Mattheo";
	console.log(localVar);
}

let globalVar = "Aizaac Ellis";

console.log(globalVar);
// console.log(localVar);

function showNames() {
	// Function scoped variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
};

showNames();

function myNewFunction () {
	let name = "Jane";

	function nestedFunction () {
		let nestedName = "John";
		console.log(name);
	};

	nestedFunction();
};

myNewFunction();


// Function and Global Scoped Variables

// Global scoped variable

let globalName = "Joy";

function myNewFunction2() {
	let nameInside = "Kenzo";

	console.log(globalName);
	console.log(nameInside);
}

myNewFunction2();

// console.log(nameInside);

// Using Alert()

alert("Hello, World!");

function showSampleAlert(){
	alert("Hello, user!");
}

// showSampleAlert();

// Using prompt()

let samplePrompt = prompt("Enter your name: ");

console.log(samplePrompt);

function printWelcomeMessage() {
	let firstName = prompt("Enter your first name: ");
	let lastName = prompt("Enter your last name: ");

	console.log(firstName + " " + lastName);
}
printWelcomeMessage();

/*
	Function naming convention
		- Function names should be definitive of the task it will perform
		- Avoid generic names
		- Avoid pointless and inappropriate function names
		- Name your functions following camel casing
		- Do not use JS reserved keywords
*/

